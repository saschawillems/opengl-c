# README #

This repository contains sources for modern OpenGL features.

All samples written with Visual Studio 2013, using glfw (http://www.glfw.org/) and glew (http://glew.sourceforge.net/).